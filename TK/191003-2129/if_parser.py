# -*- coding: utf-8 -*-
# Version:    Systemprogrammierung, WS 2019 2020, Python 3.7, 2019-10-03
# Autor:      Tim-M. Kretzschmar<mailto:t.kretzschmar@stud.hs-wismar.de>
# Copyright:  © 2019 Tim-M. Kretzschmar
# Lizenz:     Dieser Modul ist frei verwendbar. Keine Garantien!
# Kommentare: Markiert mit '###'; RB <ruediger.blach@hs-wismar.de>

r"""if_parser

  >>> ifstat(
  ...    'ifstat → "if" cond ":" block { elifpart } [ elsepart ] ;\n'
  ...  + 'elifpart → "elif" cond ":" block ;\n'
  ...  + 'elsepart → "else" ":" block ;\n'
  ...  )

"""

### Aha, Sie haben die „faule“ Lösung gewählt,
### statt zwischen 'if-stat' und 'if_stat' usw. zu unterscheiden :)!
__all__ = ['ifstat']

import logging
import scanner as s

### In meiner Lösung `if_parser.py' habe ich eine Funktion
### `parse(source)` vorgeschaltet, um zwischen der Initialisierung und
### den Parser-Regeln zu unterscheiden.

def ifstat(statements):
    """ifstat → "if" cond ":" block { elifpart } [ elsepart ] ;

    ### Da keine Fehler behandelt werden (sollen),
    ### sind Tests auf Quelltext-Fehler nicht sinnvoll!
    >>> ifstat('')

    ### Hier empfehle ich einen schmutzigen Trick:
    ### Die Aufgabenstellung lässt die Syntax von 'cond' und 'block' offen,
    ### deshalb hilft eine Vereinfachung! ( 'cond → "C" ;' ) 
    >>> ifstat('if x==3: y=4 ;')
    >>> ifstat('if x==3: y=4 elif x==4: y=5 else: y=6 ;')
    

    """

    ### Protokollierung ist nicht erforderlich,
    ### aber eventuell hilfreich.
    logging.debug("ENTER rules=%s", statements)
    s.init(statements)
    s.get_sym()
    ### `s.symbol` enthält nur die Wortart
    ### `s.token` enthält den Wert
    ### also: assert s.symbol == s.Symbol.TERMINAL and s.token == "if"
    while s.symbol != s.Symbol.END_OF_INPUT:
        if s.symbol == "if":
            s.get_sym()
            cond()
            if s.symbol == ":":
                s.get_sym()
                block()
                while s.symbol == "elif":
                    ### ***1: Erstes Lesen nach "elif" , s.u. ***2
                    s.get_sym()
                    elifpart()
                if s.symbol == "else":
                    ### s. ebenda
                    s.get_sym()
                    elsepart()
                else:
                    ### Mit `assert` entfällt die Verschachtelung!
                    s.get_sym()
            else:
                s.get_sym()
        else:
            s.get_sym()
    logging.debug("EXIT\n")

def elifpart():
    """elifpart → "elif" cond ":" block ;
    """
    logging.debug("ENTER symbol=%s", s.symbol)
    ### ***2: Zweites Lesen nach "elif", s.o. ***1
    s.get_sym()
    cond()
    if s.symbol == ":":
        s.get_sym()
        block()
    else:
        s.get_sym()
    logging.debug("EXIT next symbol=%s", s.symbol)

def elsepart():
    """elsepart → "else" ":" block ;
    """
    logging.debug("ENTER symbol=%s", s.symbol)
    s.get_sym()
    if s.symbol == ":":
        s.get_sym()
        block()
    else:
        s.get_sym()
    logging.debug("EXIT next symbol=%s", s.symbol)

def cond():
    """cond → ??? ;
    ### Vereinfachung: cond → 'C' ; 
    """
    ### Implementierung:
    ### assert s.symbol == s.Symbol.TERMINAL and s.token == "C"
    ### s.get_sym()

    pass

def block():
    """block → ??? ;
    ### s. unter `def cond():`
    """
    pass


if __name__ == "__main__":
    logging.basicConfig(
        filename='if_parser.log', filemode='w',
        format='%(module)s.%(funcName)s: %(message)s ',
        level=logging.DEBUG)
    import doctest
    doctest.testmod(raise_on_error=False)

# EOF
