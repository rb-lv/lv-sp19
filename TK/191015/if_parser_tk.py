# -*- coding: utf-8 -*-
# Version:    Systemprogrammierung, WS 2019 2020, Python 3.7, 2019-10-03
# Autor:      Tim-M. Kretzschmar<mailto:t.kretzschmar@stud.hs-wismar.de>
# Copyright:  © 2019 Tim-M. Kretzschmar
# Lizenz:     Dieser Modul ist frei verwendbar. Keine Garantien!

"""if_parser_tk

    Implementieren Sie einen Parser für die `if-elif-else`-Anweisung von
    Python! Sie können dazu den EBNF-Scanner aus der Beispiel-Sammlung
    benutzen. Verwenden Sie jedoch lediglich die Funktionen zum Lesen
    des nächsten Wortes (get_sym) und das nächste erkannte Wort (token).

    Für Bedingungen und Blöcke dürfen Sie vereinfachte Regeln nutzen:
    cond -> "C" ;
    block -> "B" ;

"""

__all__ = ['parse']

import logging
import scanner as s

def parse(source):
    """

    >>> parse("")
    >>> parse("Otto der Eroberer")
    Traceback (most recent call last):
        ...
    AssertionError
    >>> parse("if C: B")
    >>> parse("if C: B elif C: B else: B")
    >>> parse("if C: B elif C: B")
    >>> parse("if C: B else: B")

    """
    logging.debug("ENTER source=%s", source)
    s.init(source)
    s.get_sym()
    if_stat()

def if_stat():
    """if_stat → "if" cond ":" block { elif_part } [ else_part ] ;
    """
    logging.debug("ENTER token=%s", s.token)
    if s.token == "if":
        s.get_sym()
        cond()
        if s.token == ":":
            s.get_sym()
            block()
            while s.token == "elif":
                elif_part()
            if s.token == "else":
                else_part()
    s.get_sym()
    assert s.token is None
    logging.debug("EXIT\n")


def elif_part():
    """elif_part → "elif" cond ":" block ;
    """
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "elif"
    s.get_sym()
    cond()
    if s.token == ":":
        s.get_sym()
        block()
    logging.debug("EXIT next token=%s", s.token)


def else_part():
    """else_part → "else" ":" block ;
    """
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "else"
    s.get_sym()
    if s.token == ":":
        s.get_sym()
        block()
    logging.debug("EXIT next token=%s", s.token)


def cond():
    """cond → "C"
    """
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "C"
    s.get_sym()


def block():
    """block → "B" ;
    """
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "B"
    s.get_sym()


if __name__ == "__main__":
    logging.basicConfig(
        filename='if_parser.log', filemode='w',
        format='%(module)s.%(funcName)s: %(message)s ',
        level=logging.DEBUG)
    import doctest
    doctest.testmod()

# EOF
