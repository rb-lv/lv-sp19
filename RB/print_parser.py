# Time-stamp: "06-Okt-2019 00:56:06 rb at X220"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

"""Modul `print_parser`

Aufgabe:

  *print_parser*: Implementieren Sie einen Parser für Aufrufe der
   `print`-Funktion von Python 3!

   Sie können dazu den EBNF-Scanner aus der Beispiel-Sammlung
   benutzen. Verwenden Sie jedoch lediglich die Funktionen zum Lesen
   des nächsten Wortes (get_sym) und das nächste erkannte Wort
   (token).  Die Wort-Arten (symbol) des EBNF-Scanners sind
   EBNF-Wort-Arten und passen nicht zu Python 3.

   Für Parameter dürfen Sie eine vereinfachte Regel nutzen:

          param → "P" ;

Hinweis:

  - Nicht erforderlich sind Protokollierung und Fehlerbehandlung.
  - Statt einer Fehlerbehandlung sind Zusicherungen verwendbar,
    also die automatisierte Prüfung durch `assert`-Anweisungen.

Lösung:

  Wir verwenden folgende in EBNF notierte Grammatik-Regeln:

      print-call → "print" "(" param-list ")" ;
      param-list → [ param { "," param } ] ;
      param      → "P" ;

  Wir verwenden die Protokollierung mit `logger`, obwohl das nicht
  gefordert ist, um während der Entwicklung leichter Programmierfehler
  zu finden.

"""

__all__ = ["parse"]


import logging
import ebnf.scanner as s


def parse(source):
    """Syntax-Prüfung für `print`-Aufrufe von Python 3

    >>> parse("print()")
    >>> parse("print(P)")
    >>> parse("print(P, P, P, P)")

    """
    logging.debug("ENTER source=%s", source)
    s.init(source)
    s.get_sym()
    print_call()


def print_call():
    """print-call → "print" "(" param-list ")" ;"""
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "print"
    s.get_sym()
    assert s.token == "("
    s.get_sym()
    param_list()
    assert s.token == ")"
    s.get_sym()
    assert s.token is None


def param_list():
    """param-list → [ param { "," param } ] ;"""
    logging.debug("ENTER token=%s", s.token)
    if s.token == "P":  # ein Symbol voraus schauen
        param()
        while s.token == ",":
            s.get_sym()
            param()


def param():
    """param      → "P" ;"""
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "P"
    s.get_sym()


if __name__ == "__main__":
    logging.basicConfig(
        filename='print_parser.log', filemode='w',
        format='%(module)s.%(funcName)s: %(message)s ',
        level=logging.DEBUG)
    import doctest
    doctest.testmod(raise_on_error=False)


# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "de_DE")
# End:
