#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""Beispiele zu Python-Anweisungen

:version:   WS 2014/2015, Python 3
:date:      Time-stamp: <18-M�r-2019 15:55:03 rb at X1E>
:author:    R�diger Blach <http://www.wi.hs-wismar.de/~blach/>
:copyright: � 2014 R. Blach
:license:   This module has been placed in the public domain.

Das Beispiel ist sehr "gespr�chig", was in einf�hrenden Programmen
vielleicht erlaubt ist. In "richtigen" Programmen w�ren derart viele
``print``-Aufrufe ungew�hnlich. Au�erdem sind viele der Kommentare nur
f�r Anf�nger der Python-Programmierung sinnvoll und f�r
Fortgeschrittene eher st�rend.

"""


def demo():
    """Funktion zum Aufruf als Haupt-Programm (main)"""

    # pylint: disable=R0912
    # allow too many branches, e.g. 15/12

    print("""
    Demonstrationsprogramm f�r Python-Anweisungen
    =============================================
    """)

    print("""
    if-Anweisung zur einfachen Verzweigung
    --------------------------------------
    """)
    i = int(input("Geben Sie eine ganze Zahl ein: "))
    print(i, "ist", end=" ")  # Kein Zeilenumbruch
    if i % 2:    # "if" mit Alternative "else"
        print("ungerade", end=" ")
    else:
        print("gerade", end=" ")
    if i < 0:    # "if" ohne Alternative "else"
        i = -i
    print("und hat den Absolutwert", i, ".")

    print("""
    if-Anweisung zur Mehrfachverzweigung
    ------------------------------------
    """)
    # Anzahl der Tage eines Monats
    #
    # Anmerkungen:
    # - Mehrere gleiche F�lle zusammenfassen
    # - Standardfall festlegen
    month = int(input("Geben Sie die Nummer eines Monats ein: "))
    print("Monat", month, "hat", end=" ")
    if month == 2:
        days = "28 oder 29"
    elif month in (1, 3, 5, 7, 8, 10, 12):
        days = "31"
    elif month in (4, 6, 9, 11):
        days = "30"
    else:
        days = "unbekannt viele"
    print(days, "Tage.")

    print("""
    while-Schleife mit logischer Bedingung
    --------------------------------------
    """)
    # pylint: disable=C0103
    # allow p and q for variable names
    p = int(input("Geben Sie eine Primzahl ein: "))
    if p < 0:
        p = -p
    q = p // 2
    while q > 1:
        if p % q == 0:
            print(p, "hat den Faktor", q,
                  "und ist damit keine Primzahl.")
            break
        q -= 1
    else:
        print("Stimmt,", p, "ist eine Primzahl.")
    # pylint: enable=C0103

    print("""
    for-Schleife mit gegebener Durchlaufzahl
    ----------------------------------------
    """)
    # pylint: disable=C0103
    # allow n and k for variable names
    n = int(input("Fakult�t von: "))
    fak = 1
    if n <= 0:
        print("Nur f�r positive Zahlen!")
    else:
        for k in range(1, n+1):
            fak *= k
            print("{0}! = {1}".format(k, fak))
    # pylint: enable=C0103

    print("""
    Endlosschleife. Bitte mit STRG-C abbrechen!
    -------------------------------------------
    ---- Beachten Sie das Abbruchprotokoll! ---
    """)
    while True:
        pass

    # pylint: enable=R0912


if __name__ == "__main__":
    demo()

# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "de_DE")
# End:
