#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Funktion mit Seiteneffekt

:version:   WS 2014/2015, Python 3.4
:date:      Time-stamp: <11-Nov-2018 11:31:09 rb at X220>
:author:    Rüdiger Blach <http://www.wi.hs-wismar.de/~blach/>
:copyright: © 2014 Rüdiger Blach
:license:   This module has been placed in the public domain.

Das Beispiel zeigt, dass die Werte aktueller Parameter nach dem
Funktionsaufruf unverändert sind.  Hingegen wirken sich Änderungen an
durch aktuelle Parameter referenzierten Objekten über die Aufrufzeit
der Funktion hinaus aus.

"""


def changer(num, lis):
    """Ändere die Aufrufparameter!"""
    print("Start 'changer': num=", num, "lis=", lis)
    num = 3           # Lokalen Wert (Parameter) ändern
    print("Eingegebene Liste: lis=", lis)
    lis[0] = 'other'  # Seiteneffekt: Änderung des referenzierten Objekts
    print("Geändertes Listenelement: lis=", lis)
    lis = [4, 2]
    print("Geänderte Liste: lis=", lis)
    print(" Ende 'changer': num=", num, "lis=", lis)


if __name__ == "__main__":
    print("### Beispiel changer(int,list) ###")
    N = 1
    L = [2, 3]
    print("Vor dem Aufruf : N=%s L=%s" % (N, L))
    changer(N, L)
    print("Nach dem Aufruf: N=%s L=%s" % (N, L))

# EOF
