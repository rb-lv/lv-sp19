# Time-stamp: "10-Okt-2019 20:39:42 rb at X1E"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

"""Demonstration zu `None` und den Wahrheitswerten `True` und `False`

    `None` ist weder `True` noch `False`:

    >>> None == True
    False
    >>> None == False
    False

    Genauso ist 23 weder `True` noch `False`:

    >>> 23 == True
    False
    >>> 23 == False
    False

    Aber 23 ist wahr und `None` nicht:

    >>> if 23:
    ...    print("23 ist wahr")
    ... else:
    ...    print("23 ist falsch")
    ...
    23 ist wahr
    >>> if None:
    ...    print("None ist wahr")
    ... else:
    ...    print("None ist falsch")
    ...
    None ist falsch

"""

VALUES = [
    None,
    0, 23,
    "", "Otto der Eroberer",
    [], [23, 43],
    {}, {23: "··––– ···––"},
    set(), {23, 42}
]


if __name__ == "__main__":

    import doctest
    doctest.testmod()

    for value in VALUES:
        string_value = "{}".format(value)
        string_type = "{}".format(type(value))
        string = "{:20s} type: {}".format(string_value, string_type)
        if value:
            print("true:  {}".format(string))
        elif not value:
            print("false: {}".format(string))
        else:
            print("WTF:   {}".format(string))


# EOF
