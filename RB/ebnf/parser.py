# Time-stamp: "09-Okt-2019 09:21:33 rb at X1E"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

r"""Modul `parser`

Der Modul stellt Funktionen zur Analyse der EBNF-Syntax breit. Er implementiert
eine vereinfachte Variante der Version aus dem ISO-Standard 14977.

Die analysierte EBNF-Syntax lässt sich durch EBNF-Regeln beschreiben::

    ebnf       → { production ";" } ;
    production → non-terminal "→" expression ;
    expression → term { "|"  term } ;
    term       → factor { factor } ;
    factor     → non-terminal | terminal
                  | "(" expression ")"
                  | "[" expression "]"
                  | "{" expression "}"
                 ;

„Eat your own dog food“:

  >>> ebnf('\n'
  ...  + 'ebnf       → { production ";" } ;\n'
  ...  + 'production → non-terminal "→" expression ;\n'
  ...  + 'expression → term { "|"  term } ;\n'
  ...  + 'term       → factor { factor } ;\n'
  ...  + 'factor     → non-terminal | terminal\n'
  ...  + '              | "(" expression ")"\n'
  ...  + '              | "[" expression "]"\n'
  ...  + '              | "{" expression "}"\n'
  ...  + '             ;\n'
  ...  )

"""

__all__ = ['ebnf']

import logging
import ebnf.scanner as s


def error(expected):
    """Fehler-Nachricht ausgeben"""
    s.error(expected, got=s.token)


def ebnf(rules):
    """ebnf → { production ";" } ;

    >>> ebnf('')
    >>> ebnf('a→b;')

    >>> ebnf("a")
    Fehler-Position 1 (Zeile 1, Spalte 1):
        Erwartet: '→'  Erhalten: 'None'

    >>> ebnf("a→")
    Fehler-Position 2 (Zeile 1, Spalte 2):
        Erwartet: 'expression'  Erhalten: 'None'

    >>> ebnf("a→;")

    >>> ebnf("a=b;")
    Fehler-Position 3 (Zeile 1, Spalte 3):
        Erwartet: '→'  Erhalten: '='

    >>> ebnf("a→b&c;")
    Fehler-Position 5 (Zeile 1, Spalte 5):
        Erwartet: 'expression'  Erhalten: '&'

    """
    logging.debug("ENTER rules=%s", rules)
    s.init(rules)
    s.get_sym()
    while s.symbol != s.Symbol.END_OF_INPUT:
        production()
    logging.debug("EXIT\n")


def production():
    """production → non-terminal "→" expression ; """
    logging.debug("ENTER symbol=%s", s.symbol)
    if s.symbol != s.Symbol.NON_TERMINAL:
        error(s.Symbol.NON_TERMINAL)
    else:
        s.get_sym()
        if s.symbol != s.Symbol.DEFINITION:
            error(s.Symbol.DEFINITION)
        else:
            s.get_sym()
            expression()
            if s.symbol != s.Symbol.TERMINATOR:
                error("expression")
            else:
                s.get_sym()
    logging.debug("EXIT next symbol=%s", s.symbol)


def expression():
    """expression → term { "|" term } ;

    """
    logging.debug("ENTER symbol=%s", s.symbol)
    term()
    while s.symbol == s.Symbol.ALTERNATIVE:
        s.get_sym()
        term()
    logging.debug("EXIT next symbol=%s", s.symbol)


def term():
    """term → factor { factor } ;
    """
    logging.debug("ENTER symbol=%s", s.symbol)
    factor()
    factor_start = (
        s.Symbol.NON_TERMINAL,
        s.Symbol.TERMINAL,
        s.Symbol.GROUP_START,
        s.Symbol.OPTION_START,
        s.Symbol.REPETITION_START,
        )
    while s.symbol in factor_start:
        factor()
    logging.debug("EXIT next symbol=%s", s.symbol)


def factor():
    """factor → non-terminal | terminal
              | "(" expression ")"
              | "[" expression "]"
              | "{" expression "}"
              ;
    """
    logging.debug("ENTER symbol=%s", s.symbol)
    if s.symbol == s.Symbol.NON_TERMINAL or s.symbol == s.Symbol.TERMINAL:
        s.get_sym()
    elif s.symbol == s.Symbol.GROUP_START:
        s.get_sym()
        expression()
        if s.symbol == s.Symbol.GROUP_END:
            s.get_sym()
        else:
            error(s.Symbol.GROUP_END)
    elif s.symbol == s.Symbol.OPTION_START:
        s.get_sym()
        expression()
        if s.symbol == s.Symbol.OPTION_END:
            s.get_sym()
        else:
            error(s.Symbol.OPTION_END)
    elif s.symbol == s.Symbol.REPETITION_START:
        s.get_sym()
        expression()
        if s.symbol == s.Symbol.REPETITION_END:
            s.get_sym()
        else:
            error(s.Symbol.REPETITION_END)
    logging.debug("EXIT next symbol=%s", s.symbol)


if __name__ == "__main__":
    logging.basicConfig(
        filename='parser.log', filemode='w',
        format='%(module)s.%(funcName)s:%(lineno)d: %(message)s ',
        level=logging.DEBUG)
    import doctest
    doctest.testmod(raise_on_error=False)

# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "de_DE")
# End:
