# Time-stamp: "30-Sep-2019 22:11:00 rb at X1E"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

"""Paket `EBNF`

Das Paket implementiert die lexikalische und syntaktische Prüfung
von EBNF-Regeln. Es enthält folgende Module:

  - scanner:  Lexik-Prüfung
  - parser:   Syntax-Prüfung

Niklas Wirth entwickelte die „Erweiterte Backus-Nauer-Form“ (EBNF) zur
Beschreibung von Grammatiken. Später entstand ein ISO-Standard zur EBNF:
    http://www.cl.cam.ac.uk/~mgk25/iso-14977.pdf

In der Lehrveranstaltung benutzen wir eine vereinfachte Variante der
EBNF, deren Lexik im Modul `scanner` und deren Syntax im Modul
`parser` beschrieben sind.

"""

# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "de_DE")
# End:
