# Time-stamp: "16-Okt-2019 23:36:28 rb at Master3"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

"""Modul `scanner`

Der Modul stellt Funktionen zur Analyse der EBNF-Lexik breit. Er
implementiert eine vereinfachte Variante der Version aus dem
ISO-Standard 14977.

Die analysierte EBNF-Lexik besteht aus Terminalsymbolen, Hilfssymbolen
und Metazeichen:

  - *Terminalsymbole* sind Zeichenfolgen, die direkt im von der Regel
    beschriebenen Sprachkonstrukt stehen. Sie werden in einfache oder
    doppelte Anführungsstriche geschrieben, beispielsweise `"class"`
    und `'"'`.

  - *Hilfssymbole* sind Worte bzw. mit einem Bindestrich verbundene
    Worte aus ASCII-Buchstaben, beispielsweise `Klasse` oder
    `class-name`. `Über_Schrift` ist kein Hilfssymbol, da `Ü` (kein
    ASCII-Buchstabe) und der Unterstrich nicht vorkommen dürfen.

  - *Metazeichen* haben folgende Bedeutung::

    →          Definition
                z.B. `class-def → "class" class-name ":" block ;`
    ;           Terminator
                z.B. `booleans → "True" | "False" ;`
    |           Alternative
                z.B. `type-name → "int" | "float" | "bool" ;`
    ( ... )     Zusammenfassung
                z.B. `integer → natural | ( "-" natural ) ;`
    [ ... ]     Option (0 oder 1-mail)
                z.B. `integer → [ "-" ] natural ;`
    { ... }     Wiederholung (0 bis n-mal)
                z.B. `block → { statement } ;`

"""

__all__ = ['Symbol', 'symbol', 'token', 'init', 'error']

import logging
from io import TextIOWrapper
from enum import Enum, auto


class Symbol(Enum):
    """Wortarten in EBNF-Regeln"""

    END_OF_INPUT = auto()
    TERMINAL = auto()
    NON_TERMINAL = auto()
    DEFINITION = auto()
    TERMINATOR = auto()
    ALTERNATIVE = auto()
    SEQUENCE = auto()
    GROUP_START = auto()
    GROUP_END = auto()
    OPTION_START = auto()
    OPTION_END = auto()
    REPETITION_START = auto()
    REPETITION_END = auto()
    INVALID = auto()


SEPARATORS = [" ", "\t", "\n"]

META_SYMBOLS = {
    '→': Symbol.DEFINITION,
    ';': Symbol.TERMINATOR,
    '|': Symbol.ALTERNATIVE,
    ',': Symbol.SEQUENCE,
    '(': Symbol.GROUP_START,
    ')': Symbol.GROUP_END,
    '[': Symbol.OPTION_START,
    ']': Symbol.OPTION_END,
    '{': Symbol.REPETITION_START,
    '}': Symbol.REPETITION_END
    }

# pylint: disable=invalid-name
ebnf_rules = None   # Puffer mit allen Regeln
next_pos = None     # Position in `ebnf_rules`
next_char = None    # Nächstes Zeichen
next_line = None    # Zeile der EBNF-Regeln mit nächstem Zeichen
next_column = None  # Position in der Zeile mit nächstem Zeichen
token = None        # Gelesenes Wort als Zeichenfolge
symbol = None       # Wortart von `token`, Element aus `Symbol`
# pylint: enable=invalid-name


def error(expected, got):
    """Fehler-Nachricht ausgeben"""
    logging.debug("ENTER expected=%s got=%s", expected, got)
    print("Fehler-Position %d (Zeile %d, Spalte %d):"
          % (next_pos, next_line, next_column))
    if expected == Symbol.NON_TERMINAL:
        the_expected = "Ein Hilfssymbol"
    elif expected == Symbol.TERMINAL:
        the_expected = "Ein Terminalsymbol"
    elif expected in META_SYMBOLS.values():
        for key, value in META_SYMBOLS.items():
            if expected == value:
                the_expected = key
    else:
        the_expected = expected
    print("    Erwartet: '%s'  Erhalten: '%s'"
          % (the_expected, got))
    # Versuch, den Fehler zu überlesen bis hinter ';' (TERMINATOR)
    while (next_char is not None) and (next_char != ';'):
        get_char()
    get_char()
    get_sym()
    logging.debug("EXIT symbol=%s", symbol)


def init(ebnf_text):
    """Initialisiert den Scanner und puffert die EBNF-Regeln

    `ebnf_text` ist entweder eine Zeichenkette oder eine Textdatei,
    deren Inhalt in den internen Zeichen-Puffer `ebnf_rules` kopiert wird.

    >>> import scanner as s

    >>> s.init("KEINE REGEL")
    >>> print(s.ebnf_rules)
    KEINE REGEL
    >>> print(s.next_pos, s.next_line, s.next_column)
    1 1 1

    >>> import tempfile
    >>> rules_file = tempfile.TemporaryFile(mode='w+')
    >>> rules_file.write("THE RULES")  # 9 Zeichen schreiben
    9
    >>> rules_file.seek(0)  # Neue Position auf 0 setzen
    0
    >>> s.init(rules_file)
    >>> s.ebnf_rules
    'THE RULES'
    >>> print(s.next_pos, s.next_line, s.next_column)
    1 1 1
    >>> rules_file.close()

    """
    # pylint: disable=invalid-name, global-statement
    global ebnf_rules, next_pos, next_line, next_column
    # pylint: enable=invalid-name, global-statement
    logging.debug("ENTER")
    if isinstance(ebnf_text, str):
        logging.debug("ebnf_text string: %s", ebnf_text)
        ebnf_rules = ebnf_text
    elif isinstance(ebnf_text, TextIOWrapper):
        logging.debug("ebnf_text file: %s", ebnf_text)
        with ebnf_text:
            ebnf_rules = ebnf_text.read()
    else:
        logging.debug("ebnf_text: %s", ebnf_text)
        error(expected="Zeichenkette oder Datei", got=ebnf_text)
    next_pos = 0
    next_line = 1
    next_column = 0
    get_char()
    logging.debug(
        "EXIT ebnf_rules: %s",
        ebnf_rules[:23] if len(ebnf_rules) > 23 else ebnf_rules)


def get_char():
    r"""Liest das nächste Zeichen und speichert es in `next_char`

    >>> import scanner as s

    >>> line_one = "Erste Zeile"
    >>> other_line = "Noch eine Zeile"
    >>> all_lines = line_one + "\n" + other_line
    >>> s.init(all_lines)
    >>> print(s.ebnf_rules)
    Erste Zeile
    Noch eine Zeile
    >>> print(s.next_char, s.next_pos, s.next_line, s.next_column)
    E 1 1 1
    >>> while s.next_pos < len(line_one): s.get_char()
    >>> print(s.next_char, s.next_pos, s.next_line, s.next_column)
    e 11 1 11
    >>> s.get_char()
    >>> s.next_char
    '\n'
    >>> print(s.next_pos, s.next_line, s.next_column)
    12 2 0
    >>> for i in range(11): s.get_char()
    >>> print(s.next_char, s.next_pos, s.next_line, s.next_column)
    Z 23 2 11

    >>> s.init("")
    >>> s.next_char is None
    True
    >>> s.get_char()
    >>> s.next_char is None
    True

    """
    # pylint: disable=invalid-name, global-statement
    global next_char, next_pos, next_line, next_column
    # pylint: enable=invalid-name, global-statement
    logging.debug("ENTER")
    assert 0 <= next_pos <= len(ebnf_rules)
    if next_pos == len(ebnf_rules):
        next_char = None
    else:
        next_char = ebnf_rules[next_pos]
        next_pos += 1
        next_column += 1
        if next_char == "\n":
            next_line += 1
            next_column = 0
    logging.debug("EXIT next char=%s pos=%d line=%d column=%d",
                  next_char, next_pos, next_line, next_column)


def is_ascii_letter(char):
    """Ist `char` ein ASCII-Buchstabe?

    >>> is_ascii_letter(None)
    False
    >>> is_ascii_letter('@')
    False
    >>> is_ascii_letter('1')
    False
    >>> is_ascii_letter('n')
    True
    >>> is_ascii_letter('W')
    True

    """
    if char is None:
        result = False
    else:
        assert isinstance(char, str)
        assert len(char) == 1
        result = ('a' <= char <= 'z') or ('A' <= char <= 'Z')
    return result


def get_sym():
    """Ermittelt das nächste Symbol

    >>> import scanner as s

    >>> s.init('')
    >>> s.get_sym()
    >>> print(s.symbol == s.Symbol.END_OF_INPUT, s.token is None)
    True True
    >>> s.get_sym()
    >>> print(s.symbol == s.Symbol.END_OF_INPUT, s.token is None)
    True True

    >>> s.init('non-terminal → ascii-letter { "-" | ascii-letter } ;')

    >>> s.get_sym(); s.symbol == s.Symbol.NON_TERMINAL
    True
    >>> s.token
    'non-terminal'
    >>> print(s.next_line, s.next_column, s.next_char == ' ')
    1 13 True

    >>> s.get_sym(); s.symbol == s.Symbol.DEFINITION
    True
    >>> s.token
    '→'
    >>> print(s.next_line, s.next_column, s.next_char == ' ')
    1 15 True

    >>> s.get_sym(); s.symbol == s.Symbol.NON_TERMINAL
    True
    >>> s.token
    'ascii-letter'

    >>> s.get_sym(); s.symbol == s.Symbol.REPETITION_START
    True
    >>> s.token
    '{'

    >>> s.get_sym(); s.symbol == s.Symbol.TERMINAL
    True
    >>> s.token
    '"-"'

    >>> s.get_sym(); s.symbol == s.Symbol.ALTERNATIVE;
    True
    >>> s.token
    '|'

    >>> s.get_sym(); s.symbol == s.Symbol.NON_TERMINAL
    True
    >>> s.token
    'ascii-letter'

    >>> s.get_sym(); s.symbol == s.Symbol.REPETITION_END
    True
    >>> s.token
    '}'
    >>> print(s.next_line, s.next_column, s.next_char == ' ')
    1 51 True

    >>> s.get_sym(); s.symbol == s.Symbol.TERMINATOR
    True
    >>> s.token
    ';'

    >>> s.get_sym(); s.symbol == s.Symbol.END_OF_INPUT
    True
    >>> print(s.next_line, s.next_column, s.next_char == None)
    1 52 True

    Erneutes Lesen „nach dem Ende“:
    >>> s.get_sym(); s.symbol == s.Symbol.END_OF_INPUT
    True
    >>> print(s.next_line, s.next_column, s.next_char == None)
    1 52 True

    """
    # pylint: disable=invalid-name, global-statement
    global next_char, token, symbol
    # pylint: enable=invalid-name, global-statement
    logging.debug("ENTER  next char=%s pos=%d line=%d column=%d",
                  next_char, next_pos, next_line, next_column)
    while next_char in SEPARATORS:
        get_char()
    if next_char is None:
        symbol = Symbol.END_OF_INPUT
        token = None
    elif is_ascii_letter(next_char):  # Hilfssymbol
        symbol = Symbol.NON_TERMINAL
        token = next_char
        # non-terminal → ascii-letter { [ "-" ] ascii-letter }
        get_char()
        while is_ascii_letter(next_char) or next_char == '-':
            token += next_char
            get_char()
    elif next_char in ('"', "'"):  # Terminalsymbol
        symbol = Symbol.TERMINAL
        token = next_char
        # terminal → ( "'" { character } "'" ) | ( '"' { character } '"' )
        closing = next_char
        get_char()
        while (next_char is not None) and (next_char != closing):
            token += next_char
            get_char()
        token += closing
        get_char()
    elif next_char in META_SYMBOLS:  # Metazeichen
        symbol = META_SYMBOLS[next_char]
        token = next_char
        get_char()
    else:
        symbol = Symbol.INVALID
        token = next_char
        get_char()
    logging.debug("EXIT symbol=%s token=%s", symbol, token)


if __name__ == "__main__":
    FORMAT = (
        '%(asctime)s - %(name)s - %(levelname)s - '
        + '%(module)s.%(funcName)s:%(lineno)d '
        + '\n%(message)s')
    logging.basicConfig(
        filename='scanner.log', filemode='w',
        format=FORMAT,
        level=logging.DEBUG)
    logging.warning("Gestartet als Skript")
    logging.info("ANFANG")
    import doctest
    doctest.testmod(raise_on_error=False)
    logging.info("ENDE")

# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "de_DE")
# End:
