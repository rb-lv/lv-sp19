#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""**Umgekehrte Polnische Notation (UPN)**

:version:   WS 2016/2017, Python 3
:date:      Time-stamp: <07-Okt-2019 11:45:31 rb at X1E>
:author:    Rüdiger Blach <http://www.wi.hs-wismar.de/~blach/>
:copyright: © 2012-2017 Rüdiger Blach
:license:   This module has been placed in the public domain.

Aufgabe
-------

Schreiben Sie ein Python Programm,
das einen arithmetischen Ausdruck einliest
und ihn in Umgekehrter Polnischer Notation
(engl.: reverse polish notation - RPN)
wieder ausgibt!

Eingegebene arithmetische Ausdrücke
enthalten natürliche Zahlen, Variablen,
zweistellige Infix-Operatoren und Klammern.
Leerzeichen oder andere Trennzeichen
sind nicht erlaubt. Namen für Variablen
bestehen aus einem Kleinbuchstaben.
Zulässige Operatoren sind: ``+ - * / %``.

Grammatik für Ausdrücke
-----------------------
::

  expr   = term { addop term } .
  term   = factor { mulop factor } .
  factor = num | var | "(" expr ")" .
  addop  = "+" | "-" .
  mulop  = "*" | "/" | "%" .
  num    = digit { digit } .
  digit  = "0" | ... | "9" .
  var    = "a" | ... | "z" .

Beispiele
---------
::

  >>> rpn("a")
  'a'
  >>> rpn("a+b+c")
  'ab+c+'
  >>> rpn("a+b*(c-d)")
  'abcd-*+'

Lösungs-Idee: LL(1)-Parser
--------------------------

Parser analysieren die Syntax einer Eingabe.
Ein LL(1)-Parser (left lookahead 1)
liest Eingaben zeichenweise von links
und erkennt syntaktische Einheiten
mit Hilfe des ersten folgenden Zeichens.

Hinweise zur Implementierung
----------------------------

Der Modul zeigt die Verwendung
einiger Python-Werkzeuge:

- Konfiguration mittels argparse_
- Tests mittels doctest_
- Ablaufverfolgung mittels logging_
- Prüfung von Konventionen durch pylint_

Beispiele zur Prüfung mit pylint_
---------------------------------

Einen Modul prüfen::

  pylint --output-format=html rb.sp.python.rpn > /tmp/$USER-X.html
  chromium-browser /tmp/$USER-X.html

Ein Paket prüfen::

  pylint --output-format=html rb.sp > /tmp/$USER-Y.html
  chromium-browser /tmp/$USER-Y.html

Alle Python-Quelltext-Dateien eines Verzeichnisses prüfen::

  pylint --output-format=html ~/SP/*.py > /tmp/$USER-Z.html
  chromium-browser /tmp/$USER-Z.html

.. _argparse: https://docs.python.org/3/library/argparse.html
.. _doctest: https://docs.python.org/3/library/doctest.html
.. _logging: https://docs.python.org/3/library/logging.html
.. _pylint: http://www.logilab.org/card/pylint_manual

Hinweise zum Aufruf
-------------------

Anzeige der Hilfe::

  python3 -m rb.sp.python.rpn -h

Aufruf mit protokolliertem doctest_::

  python3 -m rb.sp.python.rpn -D

Ablaufverfolgung für einen Ausdruck::

  python3 -m rb.sp.python.rpn -e a+b -l info

Beobachtung der Arbeit von `getch`::

  FILTER="grep rpn.getch"
  python3 -m rb.sp.python.rpn -e a+b -l info 2>&1 | $FILTER

Beobachtung des rekursiven Abstiegs bei der Analyse::

  FILTER="grep -v -e rpn.getch -e rpn.init"
  python3 -m rb.sp.python.rpn -e a+b -l info 2>&1 | $FILTER

Quellen
-------

:see: Einführung zur `Umgekehrten Polnischen Notation bei Wikipedia
      <http://de.wikipedia.org/wiki/Umgekehrte_Polnische_Notation>`__

:see: Einführung zu `LL-Parsern bei Wikipedia
      <http://de.wikipedia.org/wiki/LL-Parser>`__

:see: Implementierung mit rekursivem Abstieg aus  Kapitel 6 in
      `Niklaus Wirth: Compilerbau - Eine Einführung, Teubner-Verlag
      <http://www.amazon.de/
      Compilerbau-Eine-Einf%C3%BChrung-Niklaus-Wirth/dp/3519323389>`__

:see: Vertiefung (nur) für angehende Compilerbauern im berühmten
      `Drachenbuch zum Compilerbau <http://www.amazon.de/
      Compilerbau-Alfred-Sethi-Jeffrey-Ullman/dp/389319150X/>`__
      (Aho & Co.: "Compilerbau - Prinzipien, Techniken und Werkzeuge",
      veröffentlicht in vielen Ausgaben beim Addison-Wesley Verlag)

Erweiterungen
-------------

- Fehlerbehandlung verbessern
- Ganze Zahlen zulassen
- Trennzeichen erlauben

.. note:: Aus Anwendersicht ist lediglich die Funktion :func:`rpn`
          zu exportieren. Alle anderen in der Variablen :data:`__all__`
          aufgezählten Attribute werden hier exportiert,
          damit sie in der Sphinx-API-Dokumentation stehen
          und in Lehrveranstaltungen anhand dieser Dokumentation
          erläutert werden können.

"""

import sys
import logging
import argparse
import doctest


__all__ = [
    'rpn',    #: Export der Schnittstellen-Funktion
    'source', 'pos', 'nextch', 'len_source',
    'init', 'state',
    'getch',
    'expr', 'term', 'factor', 'addop', 'mulop', 'num', 'var',
    'error',
    'define_arguments', 'configure_logging', 'test_module'
]


###
# # Interner Zustand des Moduls
###

# pylint: disable=C0103
#         Erlaube globale Variablen mit Namen aus Kleinbuchstaben

# Interne Zustandsvariablen
# Anm.: `None` signalisiert einen (noch) ungültigen Wert

source = None      #: Eingegebener arithmetischer Ausdruck
pos = 0            #: Position des nächsten zu lesenden Eingabe-Zeichens
nextch = None      #: Gelesenes und jetzt zu analysierendes Eingabe-Zeichen

# Hilfsvariable, um mehrfache Aufrufe von `len(source)` zu vermeiden
len_source = None  #: Anzahl der Zeichen im arithmetischen Ausdruck

# pylint: enable=C0103


def init(expression):
    """Initialisiere die Umwandlung von `expression`!

    Vor einer Umwandlung mit :func:`rpn`
    muss der interne Zustand des Moduls initialisiert werden.
    Das kann bei der Definition der globalen Variablen
    :data:`source`, :data:`pos` und :data:`nextch` geschehen.
    Für die Klarheit der Lösung
    und als Hilfsmittel für Testfälle
    ist eine Initialisierungs-Funktion hilfreich.

    >>> #import rb.sp.python.rpn as m
    >>> #from . import rpn as m
    >>> import rpn as m
    >>> m.source = m.pos = m.nextch = "old_value"
    >>> print(m.source, m.pos, m.nextch)
    old_value old_value old_value
    >>> m.init("a+b")
    >>> print(m.source, m.pos, m.nextch)
    a+b 0 None

    """
    logging.info("ENTER: expression=%s", expression)
    assert expression, "Leere Ausdrücke sind nicht erlaubt"
    global source, pos, nextch, len_source  # pylint: disable=W0603,C0103
    source = expression
    len_source = len(source)
    nextch = None
    pos = 0
    logging.info("EXIT: %s", state())


def state():
    """Formatiere internen Zustand als Zeichenfolge!

    Die Funktion hilft bei der Ablaufverfolgung mit `logging`_
    und beim Test mit `doctest`_ .

    >>> init("a"); state()
    'source=a pos=0 nextch=None'
    >>> getch(); state()
    'source=a pos=1 nextch=a'

    """
    return "source={} pos={} nextch={}".format(source, pos, nextch)


###
# # Schnittstelle zur Eingabe
###

EOI = "#"   #: 'End Of Input' Markierung


def getch():
    """Hole nächstes Zeichen aus der Quelle!

    Die Implementierung demonstriert verwendet von Zusicherungen
    zur Prüfung eines konsistenten internen Zustands.
    Außerdem beschreiben die Zusicherungen
    die Abhängigkeiten der Zustandsvariablen voneinander.

    >>> # Anfangssituation
    >>> init("a"); state()
    'source=a pos=0 nextch=None'
    >>> # Erstes Holen
    >>> getch(); state()
    'source=a pos=1 nextch=a'
    >>> # Nächsten/letztes Holen, EOI=='#'
    >>> getch(); state()
    'source=a pos=1 nextch=#'
    >>> # Erneutes Holen nach dem Ende, EOI=='#'
    >>> getch(); state() # EOI=='#'
    'source=a pos=1 nextch=#'

    """
    global nextch, pos  # pylint: disable=W0603,C0103
    # Vorbedingungen:
    # Entweder Anfangszustand oder Zwischenzustand oder Endzustand
    assert (pos == 0 and nextch is None) \
        or (0 < pos <= len_source and nextch == source[pos-1]) \
        or (pos == len_source and nextch == EOI),\
        "Fehlerhafter interner Zustand: %s" % state()
    logging.info("ENTER: %s", state())
    old_pos = pos
    if pos >= len_source:
        nextch = EOI
    else:
        nextch = source[pos]
        pos += 1
    logging.info("EXIT: %s", state())
    assert 0 < pos <= len_source, \
        "Fehlerhafter Wert von `pos`: %s" % state()
    # Nachbedingungen
    # Entweder Endzustand oder Zwischenzustand
    assert (old_pos == len_source and pos == old_pos and nextch == EOI) \
        or (old_pos < len_source and pos == old_pos+1
            and nextch == source[old_pos]),\
        "Fehlerhafter interner Zustand: %s" % state()


###
# # Rekursiver Abstieg in die Syntax-Regeln
###

# Terminalsymbole
ADDOPS = "+-"   # Additions-Operatoren
MULOPS = "*/%"  # Multiplikations-Operatoren
DIGITS = "0123456789"
LETTERS = "abcdefghijklmnopqrstuvwxyz"


def expr():
    """expr = term { addop term } ."""
    logging.info("ENTER")
    rpn_expr = term()
    while nextch in ADDOPS:
        logging.info("WHILE enter: rpn_expr=%s", rpn_expr)
        rpn_op = addop()
        rpn_term = term()
        rpn_expr += rpn_term + rpn_op
        logging.info("WHILE exit: rpn_expr=%s", rpn_expr)
    logging.info("EXIT: rpn_expr=%s", rpn_expr)
    return rpn_expr


def term():
    """term = factor { mulop factor } ."""
    logging.info("ENTER")
    rpn_term = factor()
    while nextch in MULOPS:
        logging.info("WHILE enter: rpn_term=%s", rpn_term)
        rpn_op = mulop()
        rpn_factor = factor()
        rpn_term += rpn_factor + rpn_op
        logging.info("WHILE exit: rpn_term=%s", rpn_term)
    logging.info("EXIT: rpn_term=%s", rpn_term)
    return rpn_term


def factor():
    """factor = num | var | "(" expr ")" ."""
    logging.info("ENTER")
    if nextch in DIGITS:
        rpn_factor = num()
    elif nextch in LETTERS:
        rpn_factor = var()
    elif nextch == "(":
        getch()
        rpn_factor = expr()
        if nextch == ")":
            getch()
        else:
            error(ERR_PAREN_MISSING)
    else:
        error(ERR_FACTOR)
    logging.info("EXIT: rpn_factor=%s", rpn_factor)
    return rpn_factor


def addop():
    """addop = "+" | "-" ."""
    logging.info("ENTER")
    rpn_op = nextch
    assert rpn_op in ADDOPS, \
        "Ungültiger Operator gefunden: %s" % state()
    getch()
    logging.info("EXIT: rpn_op=%s", rpn_op)
    return rpn_op


def mulop():
    """mulop = "*" | "/" | "%" ."""
    logging.info("ENTER")
    rpn_op = nextch
    assert rpn_op in MULOPS, \
        "Ungültiger Operator gefunden: %s" % state()
    getch()
    logging.info("EXIT: rpn_op=%s", rpn_op)
    return rpn_op


def num():
    """num = digit { digit } .

    Die Funktion fügt am Ende der Zahl einen Punkt ein,
    damit bei Zahlen die unmittelbar aufeinander folgen erkennbar ist,
    wo die erste Zahl endet und die nächste beginnt.
    """
    logging.info("ENTER")
    assert nextch in DIGITS, \
        "Ungültige Ziffer gefunden: %s" % state()
    rpn_num = nextch
    getch()
    while nextch in DIGITS:
        logging.info("WHILE num: rpn_num=%s", rpn_num)
        rpn_num += nextch
        getch()
    logging.info("EXIT: rpn_num=%s", rpn_num)
    return rpn_num + '.'


def var():
    """var = "a" | ... | "z" ."""
    logging.info("ENTER")
    assert nextch in LETTERS, \
        "Ungültige Variable gefunden: %s" % state()
    rpn_var = nextch
    getch()
    logging.info("EXIT: rpn_var=%s", rpn_var)
    return rpn_var


###
# # Fehlerbehandlung
###

ERR_EOI = "Eingabe ist unvollständig"
ERR_TAIL = "Überzähliges Zeichen gefunden"
ERR_PAREN_MISSING = "Schließende Klammer fehlt"
ERR_FACTOR = "Zahl, Variable oder geklammerter Ausdruck erwartet"


def error(message):
    """Bearbeite einen Fehler (Nachricht ausgeben und abbrechen)!"""
    print("***", "Fehlerhafter Ausdruck '%s'" % source)
    print(source)
    print("%s^ %s" % (' ' * (pos-1), message))
    exit(1)


###
# # Aufrufschnittstelle
###


def rpn(infix_source):
    """Wandle infix in postfix Ausdruck um!

    Die Funktion `rpn` realisiert eine
    Aufrufschnittstelle für den Modul.

    >>> rpn('a+b')
    'ab+'

    """
    logging.info("ENTER")
    init(infix_source)
    getch()
    logging.info("Initialized %s", state())
    rpn_result = expr()
    logging.info("Transformed rpn=%s %s", rpn_result, state())
    if nextch != EOI:
        error(ERR_TAIL)
    logging.info("EXIT")
    return rpn_result


###
# # Modultest
###


def define_arguments(arg_parser):
    """Argumente und Optionen des Aufrufs ermitteln"""
    arg_parser.add_argument(
        "-l", "--loglevel", dest="loglevel",
        action="store", type=str, default="warning",
        help="Set logging level", metavar="LEVEL")
    arg_parser.add_argument(
        "-d", "--doctest", dest="doctest",
        action="store_true", default=False,
        help="Run doctest")
    arg_parser.add_argument(
        "-D", "--verbose-doctest", dest="verbose_doctest",
        action="store_true", default=False,
        help="Run verbose doctest")
    arg_parser.add_argument(
        "-e", "--expression", dest="expression",
        action="store", type=str, default=None,
        help="Define source expression", metavar="EXPR")


def configure_logging(arg_parser, arguments):
    """Protokollierung konfigurieren"""
    # Gültigkeit prüfen für die gewählte Stufe zur Protokollierung
    loglevels = dict(
        critical=logging.CRITICAL,
        error=logging.ERROR,
        warning=logging.WARNING,
        info=logging.INFO,
        debug=logging.DEBUG,
        notset=logging.NOTSET
        )
    if arguments.loglevel not in loglevels.keys():
        arg_parser.print_help()
        print("*** Invalid loglevel: '%s'" % arguments.loglevel)
        print("***  Valid loglevels: %s" % loglevels.keys())
        sys.exit(1)

    # Stufe und Format der Protokollierung einstellen
    logging.basicConfig(
        level=loglevels[arguments.loglevel],
        format=('%(asctime)s %(levelname)s ' +
                '%(module)s.%(funcName)s line %(lineno)d ' +
                '%(message)s')
    )


def test_module():
    """Modul-Test durchführen

    Falls der Modul als Skript aufgerufen wird, dann führt die
    Funktion einen Modul-Test durch.  In Abhängigkeit von
    den Argumenten des Skript-Aufrufs entweder als ``doctest`` oder als
    interaktiven Test.

    Aufruf zur Anzeige möglicher Optionen::

       python3 -m rb.sp.python.rpn -h

    """
    # Optionen und Argumente verarbeiten
    parser = argparse.ArgumentParser()
    define_arguments(parser)
    args = parser.parse_args()
    configure_logging(parser, args)

    if args.doctest or args.verbose_doctest:
        # `doctest´ durchführen
        failure_count, _ = doctest.testmod(
            verbose=args.verbose_doctest)
        if failure_count > 0:
            sys.exit(1)
    else:
        # Manuellen Test durchführen
        input_message = "Arithmetischer Ausdruck      : "
        output_message = "Umgekehrte Polnische Notation: "
        if args.expression:
            # Ausdruck von der Kommandozeile verarbeiten
            print(input_message + args.expression)
            print(output_message + "%s" % rpn(args.expression))

        else:
            # Interaktiven Testlauf durchführen
            the_input = input(input_message)
            print(output_message + "%s" % rpn(the_input))


if __name__ == "__main__":
    test_module()

# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "de_DE")
# End:
