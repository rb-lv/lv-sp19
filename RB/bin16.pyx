# -*- mode: Python -*-
# Time-stamp: "15-Okt-2019 08:57:47 rb at X1E"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

# ACHTUNG 191015-084534: Um eine Quelltext-Verwaltung mit
# automatisiertem Integrationstest (CI) von GitLab (https://gitlab.com)
# zu vereinfachen, wurde die Datei mit der Endung `.pyx` versehen. Vor
# der Veröffentlichung für die Demonstration in der Einführungsvorlesung
# sollte die Endung auf `.py` geändert werden!

"""Modul 'bin16'

Aufgabenstellung: Schreiben Sie einen Modul `bin16`
mit der Funktion `binary(num: int) -> str`. Die Funktion berechnet
für die ganze Zahl `num` deren Darstellung in vier Halb-Bytes (Nibble)
codiert als vorzeichenbehaftetes Zweierkomplement!

Hinweis: http://de.wikipedia.org/wiki/Zweierkomplement

Beispiele:

    -  2 emtspricht '0000.0000.0000.0010'
    - -2 emtspricht '1111.1111.1111.1110"
    - 23 entspricht '0000.0000.0001.0111'

Lösung:

  - Test mit `doctest`
  - Prüfung mit `pylint3`
  - Implementierung mit Hilfe der Methode `str.format`,
    s. https://docs.python.org/3.7/library/string.html#format-string-syntax

Die Lösung ist für Python 3 ab Version 3.7 gedacht:

  >>> import sys
  >>> sys.version_info.major == 3
  True
  >>> sys.version_info.minor >= 7
  True

Für den Dok-Test des Moduls verlangen wir die Existenz
der Funktion `binary` im Modul `bin16`:

>>> import bin16
>>> "binary" in dir(bin16)
True
>>> type(bin16.binary)
<class 'function'>

Der Modul muss unseren Stil-Richtlinien entsprechen:

>>> import os
>>> # `pylint` liefert bei einer erfolgreichen Prüfung
>>> # den Rückkehrcode 0
>>> os.system("pylint3 -r n bin16 >/dev/null 2>&1")
0

Anmerkung: Die Lösung ist zur Demonstration in der Vorlesung gedacht
und enthält deshalb Fehler und Unzulänglichkeiten!

    - *py_compile*: Studenten dürfen im geteilten Cloud-Ordner, der
       das Beispiel enthält nicht schreiben, deshalb ist eine
       Übersetzung in diesem Ordner nicht sinnvoll. Der erzeugte
       Python-Bytecode könnte nicht gespeichert bzw. nicht mit dem
       Cloud-Speicher synchronisiert werden.

    - *pylint3*: Die erlaubte Zeilenlänge im Testsystem (Emacs für
       rb@X1E) ist verschieden vom Produktionssystem (Geany für
       blachsp@Unix-TS), deshalb bewertet `pylint` den Quelltext auf
       dem Unix-TS als verbesserungswürdig (weniger als 10 Punkte).

    - *doctest*: Vor der Veröffentlichung wurde eine
       Dok-Test-Anweisung im Beispiel „verschlimmbessert“, deshalb
       scheitert der Dok-Test. Dies demonstriert, dass in Testläufen
       erkannte Fehler eventuell fehlerhafte Tests als Ursache haben.

"""

MIN16 = -2**15   # Minimale 16-Bit-Zahl als Zweierkomplement mit Vorzeichen
MAX16 = 2**15-1  # Maximale 16-Bit-Zahl als Zweierkomplement mit Vorzeichen

# Werte: abs(MIN16) negative, MAX16 positive, 1x Null
assert 2**16 == abs(MIN16) + MAX16 + 1, "Wertebereich muss 16 Bit umfassen"


def binary(num: int) -> str:
    """Zweierkomplement als Zeichenkette aus vier Halb-Bytes darstellen

    >>> binary(0)
    '0000.0000.0000.0000'
    >>> binary(1)
    '0000.0000.0000.0001'
    >>> binary(-1)
    '1111.1111.1111.1111'
    >>> binary(23)
    '0000.0000.0001.0111'
    >>> binary(-2)
    '1111.1111.1110.1110'
    >>> binary(-23)
    '1111.1111.1110.1001'

    >>> binary(2**15-1)
    '0111.1111.1111.1111'
    >>> binary(2**15)
    Traceback (most recent call last):
    ...
    AssertionError: Zahl zu groß, Maximum ist 2**15-1

    >>> binary(-2**15)
    '1000.0000.0000.0000'
    >>> binary(-2**15-1)
    Traceback (most recent call last):
    ...
    AssertionError: Zahl zu klein, Minimum ist -2**15

    Anmerkung: Die Lösung ist relativ defensiv. Sie enthält eine Reihe
    von automatisch prüfbaren Zusicherungen (`assert`-Anweisungen).

    """

    # Wertebereich prüfen
    assert num >= MIN16, "Zahl zu klein, Minimum ist -2**15"
    assert num <= MAX16, "Zahl zu groß, Maximum ist 2**15-1"

    # Dual-Ziffern ermitteln
    dual_str = ""
    if num >= 0:
        dual_str = "{0:016b}".format(num)
        assert dual_str[0] == '0', "Positive Werte beginnen mit '0'"
    else:
        # Format 'b' liefert nur Bit-Folgen, evtl. mit negativem Vorzeichen:
        assert "{0:016b}".format(-2) == '-000000000000010', \
            "'format' arbeitet nicht wie erwartet"  # 15 Bits und '-'
        # `"{0:016b}".format(2**16 + num)` liefert die Bit-Folge
        # für die vorzeichenbehaftete Zweierkomplement-Darstellung von `num`
        assert "{0:016b}".format(2**16 + -2) == '1111111111111110', \
            "'format' arbeitet nicht wie erwartet"  # 16 Bits ohne '+'
        dual_str = "{0:016b}".format(2**16 + num)
        assert dual_str[0] == '1', "Negative Werte beginnen mit '1'"
    assert len(dual_str) == 16, \
        "'%s' ist %d Zeichen lang statt 16" % (dual_str, len(dual_str))

    # Punkte einfügen
    nibbles = (dual_str[0: 4] + "." +
               dual_str[4: 8] + "." +
               dual_str[8:12] + "." +
               dual_str[12:16])
    assert len(nibbles) == 16+3, \
        "'%s' ist %d Zeichen lang statt 16+3" % (dual_str, len(dual_str))

    return nibbles


if __name__ == "__main__":
    import doctest
    doctest.testmod()

# EOF
