# Time-stamp: "06-Okt-2019 00:55:57 rb at X220"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

r"""Modul `if_parser`

Aufgabe:

  *if_parser*: Implementieren Sie einen Parser für die
   `if-elif-else`-Anweisung von Python!

   Sie können dazu den EBNF-Scanner aus der Beispiel-Sammlung
   benutzen. Verwenden Sie jedoch lediglich die Funktionen zum Lesen
   des nächsten Wortes (`get_sym`) und das nächste erkannte Wort
   (`token`).  Die Wort-Arten (`symbol`) des EBNF-Scanners sind
   EBNF-Wort-Arten und passen nicht zu Python 3.

   Für Bedingungen und Blöcke dürfen Sie vereinfachte Regeln nutzen::

         cond  → "C" ;
         block → "B" ;

Hinweis:

  - Nicht erforderlich sind Protokollierung und Fehlerbehandlung.
  - Statt einer Fehlerbehandlung sind Zusicherungen verwendbar,
    also die automatisierte Prüfung durch `assert`-Anweisungen.

Lösung::

    if-stat   → "if" cond ":" block { elif-part } [ else-part ] ;
    elif-part → "elif" cond ":" block ;
    else-part → "else" ":" block ;
    cond      → "C" ;
    block     → "B" ;

"""

__all__ = ["parse"]

import logging
import ebnf.scanner as s


def parse(source):
    """Syntax-Prüfung für `if`-Anweisungen von Python 3

    >>> parse("if C: B")
    >>> parse("if C: B else: B")
    >>> parse("if C: B elif C: B")

    """
    logging.debug("ENTER source=%s", source)
    s.init(source)
    s.get_sym()
    if_stat()


def if_stat():
    """'if-stat → "if" cond ":" block { elif-part } [ else-part ] ;\n'"""
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "if"
    s.get_sym()
    cond()
    assert s.token == ":"
    s.get_sym()
    block()
    while s.token == "elif":
        elif_part()
    if s.token == "else":
        else_part()
    assert s.token is None


def elif_part():
    """'elif-part → "elif" cond ":" block ;\n'"""
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "elif"
    s.get_sym()
    cond()
    assert s.token == ":"
    s.get_sym()
    block()


def else_part():
    """'elif-part → "else" ":" block ;\n'"""
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "else"
    s.get_sym()
    assert s.token == ":"
    s.get_sym()
    block()


def cond():
    """cond → "C" ;\n"""
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "C"
    s.get_sym()


def block():
    """block → "B" ;\n"""
    logging.debug("ENTER token=%s", s.token)
    assert s.token == "B"
    s.get_sym()


if __name__ == "__main__":
    logging.basicConfig(
        filename='if_parser.log', filemode='w',
        format='%(module)s.%(funcName)s: %(message)s ',
        level=logging.DEBUG)
    import doctest
    doctest.testmod(raise_on_error=False)


# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "de_DE")
# End:
