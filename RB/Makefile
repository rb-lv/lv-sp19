# -*- mode: Makefile; coding: utf-8 -*-
# Time-stamp: <16-Okt-2019 14:40:02 rb at Master3>
# Version:    LV-AP, SS 2019, Python 3.7
# Author:     Rüdiger Blach <mailto:ruediger.blach@hs-wismar.de>
# Copyright:  © 2019 Rüdiger Blach
# License:    This module has been placed in the public domain.

SHELL := /bin/bash

PY_PRIVATE := \
	recursion/fac_rec.py \
	recursion/fac_loop.py \
	if_parser.py

PY_PUBLIC := \
	changer.py \
	none_and_boolean.py \
	print_parser.py \
	rpn.py \
	statements.py

PY_PUBLIC_EBNF := \
	ebnf/__init__.py \
	ebnf/parser.py \
	ebnf/scanner.py \

PY_PUBLIC_RECURSION := \
	recursion/ackermann.py \
	recursion/ackermann_demo.py \
	recursion/stack_size.py

PY_PUBLIC_ALL := $(PY_PUBLIC) $(PY_PUBLIC_EBNF) $(PY_PUBLIC_RECURSION)

OTHER_PUBLIC := \
	Makefile \
	bin16.pyx \
	rpn.doctest

PY_SOURCES := $(PY_PRIVATE) $(PY_PUBLIC_ALL)

PY_COMP_TEST := python3.7 -m py_compile
PY_DOC_TEST := python3.7 -m doctest
PY_CODE_STYLE := pycodestyle
PY_LINT := pylint3

###
### CI (Continuous Integration)
###

.PHONY: CI private public $(PY_SOURCES)

CI: private public

private: $(PY_PRIVATE)

public: $(PY_PUBLIC_ALL) bin16
	$(PY_DOC_TEST) rpn.doctest

.PHONY: bin16
bin16:
	test -r bin16.pyx
	cp bin16.pyx bin16.py
	$(PY_COMP_TEST) $@.py
	#$(PY_DOC_TEST) $@.py
	$(PY_CODE_STYLE) $@.py
	$(PY_LINT) $@.py
	rm bin16.py

$(PY_SOURCES):
	test -r $@
	$(PY_COMP_TEST) $@
	# `bin16.py` contains intentionally errors
	if [ $@ != bin16.py ]; then $(PY_DOC_TEST) $@; fi
	$(PY_CODE_STYLE) $@
	$(PY_LINT) $@

###
### CD (Continuous Delivery)
###

.PHONY: CD publish if_parser_tk
CD: publish if_parser_tk

CLOUD_SHARE := $$HOME/NextCloudPro/LV/LV-SP/SP19Share
PUBLISH_DIR := $(CLOUD_SHARE)/Beispiele
TEMP_DIR := $(shell mktemp -d)

RSYNC := rsync --quiet --archive --checksum --delete --exclude "__pycache__"

publish: public
	if [ $$PWD != $$HOME"/git/lv-sp19/RB" ]; then \
		echo not from $$PWD; \
		exit 1; \
	fi
	rsync -qa $(OTHER_PUBLIC) $(TEMP_DIR)
	rsync -qa $(PY_PUBLIC) $(TEMP_DIR)
	rsync -qa $(PY_PUBLIC_EBNF) $(TEMP_DIR)/ebnf
	rsync -qa $(PY_PUBLIC_RECURSION) $(TEMP_DIR)/recursion
	rsync -qa recursion/ackermann.log $(TEMP_DIR)/recursion/
	if [ ! -d $(PUBLISH_DIR) ]; then mkdir -p $(PUBLISH_DIR); fi
	$(RSYNC) $(TEMP_DIR)/ $(PUBLISH_DIR)/
	rm -rf $(TEMP_DIR)

IF_PARSER_TK := \
	../../TK/191015/if_parser_tk.py \
	../../TK/191015/if_parser_tk2.py \
	../../TK/191015/if_parser_tk2.diff \
	../../TK/191015/if_parser_tk2.doctest_log \
	../../TK/191015/scanner.py

IF_PARSER_TK_DIR := ../TK/191015/

if_parser_tk:
	$(RSYNC) $(IF_PARSER_TK_DIR) $(PUBLISH_DIR)/TK

# EOF
# Local Variables:
# eval: (flyspell-prog-mode)
# eval: (ispell-change-dictionary "en")
# End:
