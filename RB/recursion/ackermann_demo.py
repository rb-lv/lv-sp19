#!/usr/bin/env Python3
# -*- coding: utf-8 -*-

"""Rekursionstiefe und Fehlerbehandlung bei der Ackermannfunktion

:version:   WS 2014/2015, Python 3.4; update of WS0809, Python 2.5
:date:      Time-stamp: <12-Okt-2019 12:41:06 rb at X1E>
:author:    Rüdiger Blach <http://www.wi.hs-wismar.de/~blach/>
:credits:   Modified and extended version of a solution
            by Mareike Peter (WS0809) for Python 2.5
:copyright: © 2008-2019 Rüdiger Blach
:license:   This module has been placed in the public domain.

Lösung zur Übungsaufgabe:

  Schreiben Sie ein Programm "**ackermann.py**"
  zur Berechnung der Ackermannfunktion!

  Benutzen Sie die Variante von Rósza Péter::

      a(0,   m)   = m+1
      a(n+1, 0)   = a(n, 1)
      a(n+1, m+1) = a(n, a(n+1, m))

      'n' und 'm' sind natürlichen Zahlen

Diese Lösung testet die Funktion und gibt zusätzlich zum Funktionswert
die Anzahl rekursiver Aufrufe aus.

Außerdem kann im Fall von Laufzeitfehlern, wenn das im
Python-Interpreter eingestellte Rekursions-Maximum überschritten ist,
ein neuer Wert für die maximal erlaubte Rekursionstiefe interaktiv
eingegeben werden.

.. note:: Es gibt einen `exzellenten`_
          Wikipedia Artikel zur `Ackermannfunktion`_.

.. Aus bisher unbekanntem Grund bricht in der Python 2.5 Version das
   Programm z.B. beim Rekursionslimit 23000 für den Aufruf
   ``ackermann(4.1)`` mit ``Segmentation fault`` ab!

.. _`Ackermannfunktion`: http://de.wikipedia.org/wiki/Ackermannfunktion
.. _`exzellenten`: http://de.wikipedia.org/wiki/Wikipedia:Exzellente_Artikel

"""

# pylint: disable=invalid-name,global-statement

import sys
import traceback

# Deaktiviert um vollständige Sphinx-Dokumentation zu erhalten.
# __all__ = ['ackermann']


TRACE = False          #: Mit Ablaufprotokoll?
CALLS_PER_DOT = 10**6  #: Punkt pro `CALLS_PER_DOT` Aufrufe
DOTS_PER_LINE = 20     #: Zeilenvorschub nach `DOTS_PER_LINE` Zeilen

calls = 0      #: Anzahl von Aufrufen
depth = 0      #: Aktuelle Rekursionstiefe
max_depth = 0  #: Maximale Rekursionstiefe


def ackermann(n, m):
    """Berechne die Ackermannfunktion!

    >>> list(ackermann(0,n) for n in (0, 1, 2, 3, 4))
    [1, 2, 3, 4, 5]
    >>> list(ackermann(1,n) for n in (0, 1, 2, 3, 4))
    [2, 3, 4, 5, 6]
    >>> list(ackermann(2,n) for n in (0, 1, 2, 3, 4))
    [3, 5, 7, 9, 11]
    >>> list(ackermann(3,n) for n in (0, 1, 2, 3, 4))
    [5, 13, 29, 61, 125]
    >>> ackermann(4,0) == 2**2**2-3
    True

    Wegen zu langer Laufzeit sind folgende Tests deaktiviert::

        #>>> ackermann(4,1) == 2**2**2**2-3
        #True
        #>>> ackermann(4,2) == 2**2**2**2**2-3
        #True
        #>>> ackermann(4,3) == 2**2**2**2**2**2-3
        #True
        #>>> ackermann(4,4) == 2**2**2**2**2**2**2-3
        #True

    """
    assert (n >= 0) and (m >= 0)
    global calls, depth, max_depth
    calls += 1
    depth += 1
    if max_depth < depth:
        max_depth = depth
    if TRACE and (calls % CALLS_PER_DOT == 0):
        print(".", end="")
        sys.stdout.flush()  # pylint: disable=maybe-no-member
        if calls % (CALLS_PER_DOT * DOTS_PER_LINE) == 0:
            print()  # Zeilenvorschub
            print("{:15,} Aufrufe bei".format(calls), end=' ')
            print("aktuell {:,} und maximal {:,} Rekursionen".format(
                depth, max_depth))
            print("{:15s} Letzter Aufruf: ackermann({:d},{:d})".format(
                ' ', n, m))
    if n == 0:
        # a(0, m) = m+1
        result = m + 1
    elif m == 0:
        # a(n+1, 0) = a(n, 1)
        result = ackermann(n-1, 1)
    else:
        # a(n+1, m+1) = a(n, a(n+1, m))
        result = ackermann(n-1, ackermann(n, m-1))
    depth -= 1
    return result


def run_interactive():  # pylint: disable=too-many-branches
    """Interaktiven Test ausführen"""

    def yesno(prompt="Ja oder Nein? "):
        """Lies eine Ja-/Nein-Antwort!"""
        answer = None
        while answer is None:
            text = input(prompt)
            text = text.lower()  # Kleine oder große Buchstaben erlauben
            if text in ("j", "ja", "y", "yes"):
                answer = True
            elif text in ("n", "nein", "no"):
                answer = False
            else:
                print("Bitte mit 'ja' oder 'nein' antworten!")
        return answer

    def read_nat(prompt="Geben Sie eine natürliche Zahl ein > "):
        """Lies eine natürliche Zahl!"""
        got_nat = False
        while not got_nat:
            nat = int(input(prompt))
            if nat >= 0:
                got_nat = True
            else:
                print("%d ist keine natürliche Zahl!" % nat)
        return nat

    def run_test(n, m):
        """Testlauf von ackermann(n, m)"""
        try:
            return ackermann(n, m)
        except RuntimeError:
            print("Laufzeitfehler in 'ackermann({:d},{:d})'".format(n, m))
            print("nach insgesamt {:,d} Aufrufen".format(calls))
            print("bei maximal {:,d} rekursiven Aufrufen".format(max_depth))
            SEGMENTS = 5    # Nur letzte Aufrufsegmente
            print("mit den letzten {:d} Aufrufsegmenten:".format(SEGMENTS))
            traceback.print_exc(SEGMENTS)
            change_recursion_limit = yesno(
                "Die erlaubte Rekursionstiefe ist " +
                str(sys.getrecursionlimit()) + "\n" +
                "Wollen Sie diesen Wert ändern? ")
            if change_recursion_limit:
                new_recursion_limit = read_nat("Neuer Wert: ")
                sys.setrecursionlimit(new_recursion_limit)
                change_recursion_limit = False
            return None
        finally:
            if TRACE and (calls > CALLS_PER_DOT):
                print()  # Ablaufprotokoll abschließen

    run = yesno("Interaktive Tests durchführen? ")
    if run:
        global TRACE
        TRACE = yesno("Ablaufprotokoll einschalten? ")
    while run:
        n = read_nat("Erster  Parameter: ")
        m = read_nat("Zweiter Parameter: ")
        print("=== ackermann(%d,%d) ===" % (n, m))
        global calls, depth, max_depth
        calls = depth = max_depth = 0
        result = run_test(n, m)
        if result is not None:
            print("Ergebnis: {:15,}".format(result))
            print("Aufrufe : {:15,}".format(calls))
            assert depth == 0
            print("Maximal erreichte Rekursionstiefe: {:,}".format(max_depth))
        run = yesno("Nochmal? ")


def run_doctest():
    """'doctest' ausführen"""
    import doctest
    (failures, tests) = doctest.testmod()
    print("%d von %d Tests gescheitert" % (failures, tests))
    if failures > 0:
        sys.exit(1)


if __name__ == "__main__":

    print("Test einiger Spezialfälle:")
    run_doctest()

    print("Interaktiver Test:")
    run_interactive()

# EOF
