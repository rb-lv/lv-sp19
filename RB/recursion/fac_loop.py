# Time-stamp: "16-Okt-2019 14:58:07 rb at Master3"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

"""Aufgabe `fac_loop`

Aufgabenstellung:

    Implementieren Sie im Modul `~/SP19/fac_loop.py` eine
    Fakultätsfunktion `factorial(n: int) -> int` für natürliche Zahlen
    mit Hilfe einer Schleife und ohne rekursive Aufrufe!


    >>> for n in range(6): print("{}!={}".format(n, factorial(n)))
    0!=1
    1!=1
    2!=2
    3!=6
    4!=24
    5!=120
    >>> factorial(23)
    25852016738884976640000

"""


def factorial(n):  # pylint: disable=invalid-name
    """Fakultät nicht-rekursiv berechnen"""
    assert isinstance(n, int)
    assert n >= 0
    result = 1
    for i in range(1, n+1):
        result *= i
    return result


if __name__ == "__main__":
    import doctest
    doctest.testmod()

# EOF
