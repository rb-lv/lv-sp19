# Time-stamp: "16-Okt-2019 23:44:31 rb at Master3"
# Version   : Python >= 3.7, WS 2019/2020
# Author    : Rüdiger Blach
# Contact   : ruediger.blach@hs-wismar.de
# Licence   : This module has been placed in the public domain.

"""Aufgabe 'fac_rec'

Aufgabenstellung:

    Implementieren Sie im Modul ~/SP19/fac_rec.py eine rekusive
    Fakultätsfunktion `factorial(n: int) -> int` für natürliche Zahlen!

    >>> for n in range(6): print("{}!={}".format(n, factorial(n)))
    0!=1
    1!=1
    2!=2
    3!=6
    4!=24
    5!=120
    >>> factorial(23)
    25852016738884976640000


"""


def factorial(n):  # pylint: disable=invalid-name
    """Fakultät rekursiv berechnen"""
    assert isinstance(n, int)
    assert n >= 0
    # pylint: disable=no-else-return
    if n in (0, 1):
        return 1
    else:
        return n * factorial(n-1)


if __name__ == "__main__":
    import doctest
    doctest.testmod()

# EOF
