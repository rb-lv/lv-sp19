#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Das Programm experimentiert mit der Größe des Laufzeitkellers.

:version:   WS 2016/2017, Python 3
:date:      Time-stamp: <12-Okt-2019 13:24:08 rb at X1E>
:author:    Rüdiger Blach <http://www.wi.hs-wismar.de/~blach/>
:copyright: © 2007-2016 Rüdiger Blach
:license:   This module has been placed in the public domain.

Das Hauptprogramm :func:`master` startet Unterprogramme :func:`slave`.
Unterprogramme starten die unendlich rekursive Funktion
:func:`recursion` und brechen mit einem 'Segmentation fault' ab.  Sie
zeigen Zwischenwerte zur Tiefe der Rekursion an. Die vom
Python-Interpreter erlaubte Rekursionstiefe und die Größe des
Laufzeitkellers werden während der Testläufe sukzessive erhöht.

.. note:: Die Änderungen der Größe des Laufzeitkellers
          mit :func:`threading.stack_size()` haben scheinbar
          keinen Einfluss zur Laufzeit (Ubuntu 12.04 mit Python 3.4.2).
          Vielmehr scheint das setzen der Keller-Größe im Shell-Prozess
          mit ``ulimit -s <VALUE>`` die Laufzeit zu beeinflussen.

.. note:: Auch in Ubuntu 18.04 kann nur der Administrator (~root) die
          Keller-Größe mit ``ulimit -s <VALUE>`` ändern. [191012-132208]

"""

import sys
import os
import threading


TRACE = False


def recursion(step):
    """Rekursive Aufrufe bis zum Keller-Überlauf"""
    try:
        if TRACE and (step % 1000 == 0):
            print('.', end=" ")
            if step % (10 * 1000) == 0:
                print("{:6d}".format(step))
        recursion(step + 1)
    except RuntimeError:
        print("***", end=" ")
        print("Laufzeitfehler bei Rekursionsschritt {}".format(step))


def print_stack_size():
    """Größe des Laufzeitkellers ausgeben"""
    name = threading.current_thread().getName()
    print("{}: ".format(name), end="")
    ident = threading.get_ident()
    stack_size = threading.stack_size()
    print("ident={} ; stack_size={}".format(ident, stack_size))


def slave(size):
    """Unterprogramm für Experimente

    Das Programm läuft bis 'Segmentation fault'.

    :param int size: Größe des Laufzeitkellers in Byte
    """
    threading.stack_size(size)
    print_stack_size()
    while True:
        old = sys.getrecursionlimit()
        print("Altes Rekursionslimit: {}".format(old))
        new = 2 * old
        sys.setrecursionlimit(new)
        print("Neues Rekursionslimit: {}".format(new))
        runner = threading.Thread(target=recursion, args=(1,))
        runner.start()
        runner.join()


def master():
    """Hauptprogramm zur Steuerung"""
    print("MASTER:")
    print_stack_size()
    page_size = 4096
    for i in range(4):
        size = i * 10 * page_size
        print("SLAVE: {:d} KB".format(size))
        os.system("python3 -m stack_size {:d}".format(size))


if __name__ == "__main__":
    if len(sys.argv) == 1:
        master()
    else:
        assert len(sys.argv) == 2
        slave(int(sys.argv[1]))

# EOF
