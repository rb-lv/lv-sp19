#!/usr/bin/env Python3
# -*- coding: utf-8 -*-

"""Implementierungen der Ackermannfunktion

:version:   WS 2014/2015, Python 3.4; update of WS0809, Python 2.5
:date:      Time-stamp: <12-Okt-2019 12:34:11 rb at X1E>
:author:    Rüdiger Blach <http://www.wi.hs-wismar.de/~blach/>
:credits:   Modified and extended version of a solution
            by Mareike Peter (WS0809) for Python 2.5
:copyright: © 2008-2019 Rüdiger Blach
:license:   This module has been placed in the public domain.

Ackermannfunktion nach Rósza Péter::

      a(0,   m)   = m+1
      a(n+1, 0)   = a(n, 1)
      a(n+1, m+1) = a(n, a(n+1, m))

      'n' und 'm' sind natürlichen Zahlen

.. note:: Es gibt einen `exzellenten`_
          Wikipedia Artikel zur `Ackermannfunktion`_.

.. _`Ackermannfunktion`: http://de.wikipedia.org/wiki/Ackermannfunktion
.. _`exzellenten`: http://de.wikipedia.org/wiki/Wikipedia:Exzellente_Artikel

Der Modul implementiert zwei Varianten der Ackermannfunktion:

- :func:`ackermann_pure`: Einfache rekursive Implementierung ohne Zusätze
- :func:`ackermann_cached`: Implementierung mit Zwischenspeicherung

Eine interaktive Variante der Implementierung realisiert der Modul
`ackermann_demo`.

"""

import logging

log = logging.getLogger(__name__)  # pylint: disable=invalid-name
log.addHandler(logging.StreamHandler())


def log_init(info_step=10**3):
    """Initialisierung der Protokollierung

    :param int info_step: Aufrufe bis eine Zwischen-Ausgabe erfolgt
    """
    log.calls = 0    #: Anzahl der Aufrufe insgesamt
    log.rec_cur = 0  #: Aktuelle Anzahl rekursiver Aufrufe
    log.rec_max = 0  #: Maximale Anzahl rekursive Aufrufe
    log.cached = 0   #: Anzahl der im Zwischenspeicher gespeicherten Werte
    log.found = 0    #: Anzahl der im Zwischenspeicher gefundenen Werte
    log.info_step = info_step  #: Zwischen-Ausgaben nach `info_step`


def log_enter(n, m):  # pylint: disable=invalid-name
    """Protokoll beim Eintritt in die Ackermannfunktion"""
    if log.isEnabledFor(logging.INFO):
        if log.isEnabledFor(logging.DEBUG):
            depth = '-' * log.rec_cur + '>'
            log.debug("%s ENTER ackermann(%d,%d)", depth, n, m)
        log.rec_cur += 1
        if log.rec_cur > log.rec_max:
            log.rec_max = log.rec_cur
        log.calls += 1
        if log.calls % log.info_step == 0:
            log.info("%15d Aufrufe bisher", log.calls)
            log.info("%15s  %12d aktuell rekursiv; %7d maximal",
                     ' ', log.rec_cur, log.rec_max)
            if log.cached > 0:
                log.info("%15s  %12d gespeichert; %12d gefunden",
                         ' ', log.cached, log.found)


def log_exit(n, m, result):   # pylint: disable=invalid-name
    """Protokoll beim Beenden eines Aufrufs der Ackermannfunktion"""
    if log.isEnabledFor(logging.INFO):
        log.rec_cur -= 1
        if log.isEnabledFor(logging.DEBUG):
            depth = '<' + '-' * log.rec_cur
            log.debug("%s EXIT ackermann(%d,%d)=%d", depth, n, m, result)
            if log.cached > 0:
                for i in range(CACHE_N):
                    log.debug("%d %s", i, cache[i])


def ackermann_pure(n, m):  # pylint: disable=invalid-name
    """Berechne rekursiv die Ackermannfunktion!

    Die Implementierung hat keine Zusätze.

    >>> list(ackermann_pure(0,n) for n in (0, 1, 2, 3, 4))
    [1, 2, 3, 4, 5]
    >>> list(ackermann_pure(1,n) for n in (0, 1, 2, 3, 4))
    [2, 3, 4, 5, 6]
    >>> list(ackermann_pure(2,n) for n in (0, 1, 2, 3, 4))
    [3, 5, 7, 9, 11]
    >>> list(ackermann_pure(3,n) for n in (0, 1, 2, 3, 4))
    [5, 13, 29, 61, 125]
    >>> ackermann_pure(4,0) == 2**2**2-3
    True

    """
    ackermann = ackermann_pure
    # pylint: disable=no-else-return
    if n == 0:
        # a(0, m) = m+1
        return m + 1
    elif m == 0:
        # a(n+1, 0) = a(n, 1)
        return ackermann(n-1, 1)
    else:
        # a(n+1, m+1) = a(n, a(n+1, m))
        return ackermann(n-1, ackermann(n, m-1))
    # pylint: enable=no-else-return


CACHE_N = 9  #: Maximum für gespeicherte `n`
# pylint: disable=invalid-name
cache = {}   #: Verzeichnis mit Index `n` von Verzeichnissen mit Index `m`
# pylint: enable=invalid-name


def init_cache():
    """Zwischenspeicher initialisieren"""
    for n_entry in range(CACHE_N + 1):
        cache[n_entry] = {'m_keys': []}  # Liste zwischengespeicherter `m`


def ackermann_cached(n, m):  # pylint: disable=invalid-name
    """Berechne die Ackermannfunktion mit Hilfe eines Zwischenspeichers!

    >>> init_cache()
    >>> list(ackermann_cached(0,n) for n in (0, 1, 2, 3, 4))
    [1, 2, 3, 4, 5]
    >>> list(ackermann_cached(1,n) for n in (0, 1, 2, 3, 4))
    [2, 3, 4, 5, 6]
    >>> list(ackermann_cached(2,n) for n in (0, 1, 2, 3, 4))
    [3, 5, 7, 9, 11]
    >>> list(ackermann_cached(3,n) for n in (0, 1, 2, 3, 4))
    [5, 13, 29, 61, 125]
    >>> ackermann_cached(4,0) == 2**2**2-3
    True

    """
    ackermann = ackermann_cached
    if log.isEnabledFor(logging.INFO):
        log_enter(n, m)
    if m in cache[n]['m_keys']:
        if log.isEnabledFor(logging.INFO):
            log.found += 1
        result = cache[n][m]
    else:
        if n == 0:
            # a(0, m) = m+1
            result = m + 1
        elif m == 0:
            # a(n+1, 0) = a(n, 1)
            result = ackermann(n-1, 1)
        else:
            # a(n+1, m+1) = a(n, a(n+1, m))
            result = ackermann(n-1, ackermann(n, m-1))
        cache[n]['m_keys'].append(m)
        cache[n][m] = result
        if log.isEnabledFor(logging.INFO):
            log.cached += 1
    if log.isEnabledFor(logging.INFO):
        log_exit(n, m, result)
    return result


def main():
    """Hauptprogramm"""

    import argparse
    import doctest
    import sys

    def define_arguments(arg_parser):
        """Argumente des Aufrufs definieren"""
        arg_parser.add_argument(
            "-d", "--doctest", dest="doctest",
            action="store_true", default=False,
            help="run doctest")
        arg_parser.add_argument(
            "-D", "--verbose-doctest", dest="verbose_doctest",
            action="store_true", default=False,
            help="run verbose doctest")
        arg_parser.add_argument(
            "-l", "--loglevel", dest="loglevel",
            action="store", type=str, default=None,
            choices=("DEBUG", "INFO"),
            help="set logging level")
        arg_parser.add_argument(
            "-s", "--step", dest="step",
            action="store", type=int, default=10**6,
            help="number of calls per INFO")
        arg_parser.add_argument(
            "n", type=int, help="argument 'n' in 'ackermann(n,m)'")
        arg_parser.add_argument(
            "m", type=int, help="argument 'm' in 'ackermann(n,m)'")
        ackermann_group = arg_parser.add_mutually_exclusive_group()
        ackermann_group.add_argument(
            "-p", "--pure", action="store_true",
            help="ackermann recursive without extras")
        ackermann_group.add_argument(
            "-r", "--recursive", action="store_true",
            help="ackermann recursive without logging")
        ackermann_group.add_argument(
            "-c", "--cached", action="store_true",
            help="ackermann cached with logging")

    parser = argparse.ArgumentParser()
    define_arguments(parser)
    args = parser.parse_args()

    if args.doctest or args.verbose_doctest:
        (failures, _) = doctest.testmod(verbose=args.verbose_doctest)
        if failures > 0:
            sys.exit(1)

    if args.loglevel:
        log.setLevel(args.loglevel)
        log_init(args.step)
    else:
        log.setLevel(logging.WARNING)

    sys.setrecursionlimit(10**8)
    assert (args.n >= 0) and (args.m >= 0)
    assert args.n < CACHE_N

    if args.pure:
        ackermann = ackermann_pure
    if args.cached:
        ackermann = ackermann_cached
        init_cache()
    else:  # Standard
        ackermann = ackermann_pure
    print("ackermann({},{}) = {}".format(
        args.n, args.m, ackermann(args.n, args.m)))
    if log.isEnabledFor(logging.INFO) and log.calls > 0:
        log.info("Aufrufe: %d insgesamt; maximal %d rekursive",
                 log.calls, log.rec_max)
        log.info("gespeichert: %d; gefunden: %d",
                 log.cached, log.found)


if __name__ == "__main__":

    main()

# EOF
